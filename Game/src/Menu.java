import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.layout.VBox;

public class Menu extends VBox {
    Menu(Button ... buttons){
        setSpacing(15);
        setAlignment(Pos.CENTER);
        setTranslateX(Game.WIDTH/2-120);
        setTranslateY(Game.HEIGHT/2-100);
        getChildren().addAll(buttons);
    }
}