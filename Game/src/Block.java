import javafx.geometry.Rectangle2D;
import javafx.scene.image.Image;
import javafx.scene.layout.Pane;
import javafx.scene.image.*;

public class Block extends Pane {

    ImageView block = new ImageView(new Image(getClass().getResourceAsStream("Images/objects.png")));
    public enum BlockType {
        PLATFORM, BRICK, LET, MONSTER
    }
    BlockType blockType;
    double block_size, startX;

    Block(BlockType blockType, int x, int y){
        startX = x;
        this.blockType = blockType;
        switch (blockType){
            case LET:
                block.setViewport(new Rectangle2D(176, 288, 15, 15));
                block_size = Game.BLOCK_SIZE / 2;
                block.setFitWidth(block_size);
                block.setFitHeight(block_size);
                setTranslateX(x);
                setTranslateY(y + block_size);
                break;
            case BRICK:
                block.setViewport(new Rectangle2D(0, 0, 15, 15));
                block_size = Game.BLOCK_SIZE;
                block.setFitWidth(block_size);
                block.setFitHeight(block_size);
                setTranslateX(x);
                setTranslateY(y);
                break;
            case MONSTER:
                block.setViewport(new Rectangle2D(64, 336, 16, 16));
                block_size = Game.BLOCK_SIZE / 2;
                block.setFitWidth(block_size);
                block.setFitHeight(block_size);
                setTranslateX(x);
                setTranslateY(y + block_size);
                break;
            case PLATFORM:
                block.setViewport(new Rectangle2D(15, 0, 33, 15));
                block_size = Game.BLOCK_SIZE;
                block.setFitWidth(block_size);
                block.setFitHeight(block_size);
                setTranslateX(x);
                setTranslateY(y);
                break;
        }
        getChildren().add(block);
        Game.arrBlock.add(this);
    }
    void moveX(double x){
        int LeftRight = x > 0 ? 1 : -1;
        for(int i = 0; i < Math.abs(x); i++){
            setTranslateX(getTranslateX() + LeftRight);
        }
    }
}
