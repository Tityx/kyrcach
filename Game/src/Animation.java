import javafx.animation.Transition;
import javafx.geometry.Rectangle2D;
import javafx.scene.image.ImageView;
import javafx.util.Duration;

public class Animation extends Transition{

    private ImageView imageView;
    private int columns;
    private int offsetX;
    private int offsetY;
    private int width;
    private int height;

    Animation(ImageView imageView, Duration duration, int columns, int offsetX, int offsetY, int width, int height){
        this.imageView = imageView;
        this.columns = columns;
        this.offsetX = offsetX;
        this.offsetY = offsetY;
        this.width = width;
        this.height = height;
        setCycleDuration(duration);
    }

    void setOffsetY(int offsetY){
        this.offsetY = offsetY;
    }
    void setColumns(int columns){
        this.columns = columns;
    }

    protected void interpolate(double k){
        int index = (int)Math.floor(columns * k);
        int x = index * width + offsetX;
        int y = offsetY;
        imageView.setViewport(new Rectangle2D(x, y, width, height));
    }
}
