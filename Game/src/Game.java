import javafx.animation.*;
import javafx.application.*;
import javafx.event.EventHandler;
import javafx.scene.*;
import javafx.scene.control.*;
import javafx.scene.image.*;
import javafx.scene.input.*;
import javafx.scene.layout.*;
import javafx.scene.paint.*;
import javafx.scene.text.Font;
import javafx.stage.*;

import javax.swing.*;
import java.io.*;
import java.net.*;
import java.util.*;

public class Game extends Application {

    public static final int WIDTH = 1200, HEIGHT = 600;
    public static final int BLOCK_SIZE = 50, PLAYER_SIZE = 40;
    public static ArrayList<Block> arrBlock = new ArrayList<>();
    private ImageView imageView1 = new ImageView(new Image(getClass().getResourceAsStream("Images/sprites.png")));
    private ImageView imageView2 = new ImageView(new Image(getClass().getResourceAsStream("Images/sprites2.png")));
    /*MENU*/
    private Pane rootStartDisplay = new Pane();
    private Scene sceneStartDisplay = new Scene(rootStartDisplay, WIDTH, HEIGHT);
    private Button btStartSoloGame = new Button("Однопользовательская игра"),
            btStart2Players1Computer = new Button("Мультиплеер на 1 компьютере");
    private Menu menu = new Menu(btStartSoloGame, btStart2Players1Computer);
    /*SOLO MODE*/
    public static Pane rootSoloMode = new Pane();
    private Scene sceneSoloMode = new Scene(rootSoloMode, WIDTH, HEIGHT);
    /*COOP MODE*/
    public static Pane rootCoopMode = new Pane();
    private Scene sceneCoopMode = new Scene(rootCoopMode, WIDTH, HEIGHT);
    /*RESULTS*/
    public static Pane rootResults = new Pane();
    private Scene sceneResults = new Scene(rootResults, WIDTH, HEIGHT);
    /*BACKGROUNDS*/
    private ImageView background = new ImageView(new Image(getClass().getResourceAsStream("Images/sky2.png")));
    private ImageView backGameOver = new ImageView(new Image(getClass().getResourceAsStream("Images/back_game_over.png")));
    /*LABELS*/
    private Label distanse = new Label("");
    private Label playerInfo = new Label("");
    private Label pressSpace = new Label("Press SPACE to go to menu");
    private Label lblResults = new Label("");
    /*PLAYERS*/
    private Character player = new Character(imageView1, 5, 1, 0, 0, 160, 85, 100),
            player2 = new Character(imageView2, 9, 9, 0, 925, 790, 120, 110);
    /*CLIENT-SERVER*/
    private Socket client;
    private ObjectOutputStream out;
    private ObjectInputStream in;
    private Thread t;
    /*COMMANDS*/
    private String cmExit = "quite", cmSendRecords = "send records";
    /**/
    private String records;
    private int sceneNumber = 0;
    private double movementSpeed = 5;
    private long startTime = 0, prevTime = 0;
    private MonsterAI monsterAI;
    private int maxDistance;
    private double prevDistancePlayer, nextDistancePlayer, currDistancePlayer, prevDistancePlayer2, nextDistancePlayer2, currDistancePlayer2;

    public static void main(String[] args){
        launch(args);
    }

    public void start(Stage stage){
        controller(stage);
        stage.setTitle("Best game in the world!!!");
        stage.setScene(sceneStartDisplay);
        background.setFitHeight(HEIGHT);
        background.setOpacity(0.5);
        rootStartDisplay.getChildren().addAll(background, menu);
        stage.show();
        distanse.setFont(new Font(20));
        playerInfo.setTranslateX(400);
        playerInfo.setTranslateY(200);
        playerInfo.setFont(new Font(40));
        playerInfo.setTextFill(Color.web("red"));
        pressSpace.setTranslateX(400);
        pressSpace.setTranslateY(400);
        pressSpace.setFont(new Font(40));
        pressSpace.setTextFill(Color.web("red"));
        lblResults.setTranslateX(900);
        lblResults.setTranslateY(100);
        lblResults.setFont(new Font(20));
        lblResults.setTextFill(Color.web("black"));

        try {
            client = new Socket("localhost", 1211);
            out = new ObjectOutputStream(client.getOutputStream());
            in = new ObjectInputStream(client.getInputStream());
        } catch (IOException e) {
            e.printStackTrace();
        }
        t = new Thread(() -> {
            while(true){
                try {
                    String tmp = (String) in.readObject();
                    if(tmp.equals(cmSendRecords)){
                        records = (String) in.readObject();
                    }
                } catch (EOFException e) {
                    //it's fine
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (ClassNotFoundException e) {
                    e.printStackTrace();
                }
            }
        });
        t.start();

        AnimationTimer timer = new AnimationTimer() {
            @Override
            public void handle(long now) {
                if(sceneNumber == 1) {
                    updateSoloGame(stage);
                }
                if(sceneNumber == 2){
                    updateCoopGame(stage);
                }
            }
        };
        timer.start();
    }

    void controller(Stage stage){
        stage.setOnCloseRequest(event -> {
            sendData(cmExit);
            System.exit(0);
        });

        btStartSoloGame.setOnAction(event -> {
            buildScene(stage, rootSoloMode, sceneSoloMode);
            rootSoloMode.getChildren().addAll(player, distanse);
            player.setRoot(rootSoloMode);
            player.setTranslateX(50);
            player.setTranslateY(510);
            rootSoloMode.setTranslateX(0);
            distanse.setTranslateX(1000);
            distanse.setTranslateY(100);
            sceneNumber = 1;
            rootStartDisplay.getChildren().clear();
            startTime = System.currentTimeMillis();
            currDistancePlayer = 0;
        });

        sceneSoloMode.setOnKeyPressed(event -> {
            if(event.getCode() == KeyCode.UP){
                player.jump();
            }
            if(event.getCode() == KeyCode.ESCAPE){
                go_to_menu(stage);
            }
        });

        btStart2Players1Computer.setOnAction(event ->{
            buildScene(stage, rootCoopMode, sceneCoopMode);
            rootCoopMode.getChildren().addAll(player, player2, distanse);
            player.setRoot(rootCoopMode);
            player.setTranslateX(50);
            player.setTranslateY(510);
            player2.setRoot(rootCoopMode);
            player2.setTranslateX(50);
            player2.setTranslateY(510);
            rootCoopMode.setTranslateX(0);
            distanse.setTranslateX(1000);
            distanse.setTranslateY(100);
            sceneNumber = 2;
            rootStartDisplay.getChildren().clear();
            startTime = System.currentTimeMillis();
        });

        sceneCoopMode.setOnKeyPressed(event -> {
            if(event.getCode() == KeyCode.UP){
                player.jump();
            }
            if(event.getCode() == KeyCode.W){
                player2.jump();
            }
            if(event.getCode() == KeyCode.ESCAPE){
                go_to_menu(stage);
            }
        });

        sceneResults.setOnKeyPressed(event -> {
            if(event.getCode() == KeyCode.SPACE){
                go_to_menu(stage);
            }
        });
    }
    void buildScene(Stage stage, Pane root, Scene scene){
        background.setOpacity(1);
        root.getChildren().add(background);
        stage.setScene(scene);
        int randLevel = new Random().nextInt(1);
        Level level = new Level(randLevel, root);
        monsterAI = new MonsterAI();
        monsterAI.works = true;
        int levelLength = Level.levels[randLevel][Level.levels[randLevel].length - 1].length();
        maxDistance = levelLength * BLOCK_SIZE - WIDTH/3*2;
        background.setFitWidth(levelLength * BLOCK_SIZE + 10);
    }

    void updateSoloGame(Stage stage){
        prevDistancePlayer = player.getTranslateX();
        long currentTime = System.currentTimeMillis() - startTime;
        long step = currentTime - prevTime;
        if(step > 1000){
            prevTime = currentTime;
            if((int)(currentTime / 1000) % 10 == 0) movementSpeed++;
        }
        distanse.setText("Metres: " + (int)(currDistancePlayer / 100));
        player.animation.play();
        player.moveX(movementSpeed);
        if(player.gravity < 10){
            player.gravity++;
        }
        player.moveY(player.gravity);
        nextDistancePlayer = player.getTranslateX();
        currDistancePlayer += nextDistancePlayer - prevDistancePlayer;
        if(player.getTranslateY() > Game.HEIGHT || player.isDie){
            monsterAI.works = false;
            sendData(cmSendRecords);
            String nameResult = JOptionPane.showInputDialog("Введите своё имя");
            sendData(nameResult);
            sendData((int)currDistancePlayer/100);
            player.isDie = false;
            movementSpeed = 5;
            playerInfo.setText("Your distanse: " + (int)(currDistancePlayer / 100) + " metres!");
            go_to_results(stage);
            return;
        }
        double x = player.getTranslateX();
        int step_ = WIDTH / 3;
        if(x >= maxDistance){
            player.setTranslateX(step_);
            rootSoloMode.setTranslateX(0);
        }
        if(x > step_) {
            rootSoloMode.setTranslateX(-(x - step_));
            distanse.setTranslateX(x + WIDTH/2);
        }
    }

    void updateCoopGame(Stage stage) {
        prevDistancePlayer = player.getTranslateX();
        prevDistancePlayer2 = player2.getTranslateX();
        long currentTime = System.currentTimeMillis() - startTime;
        long step = currentTime - prevTime;
        if(step > 1000){
            prevTime = currentTime;
            if((int)(currentTime / 1000) % 10 == 0) movementSpeed++;
        }
        distanse.setText("Metres \nFirst player: " + (int)(currDistancePlayer / 100) + "\nSecond player: " + (int)(currDistancePlayer2 / 100));
        player.animation.play();
        player2.animation.play();
        player.moveX(movementSpeed);
        player2.moveX(movementSpeed);
        if(player.gravity < 10){
            player.gravity++;
        }
        if(player2.gravity < 10){
            player2.gravity++;
        }
        player.moveY(player.gravity);
        player2.moveY(player2.gravity);
        nextDistancePlayer = player.getTranslateX();
        currDistancePlayer += nextDistancePlayer - prevDistancePlayer;
        nextDistancePlayer2 = player2.getTranslateX();
        currDistancePlayer2 += nextDistancePlayer2 - prevDistancePlayer2;
        if(player.getTranslateY() > Game.HEIGHT || player.isDie){
            monsterAI.works = false;
            player.isDie = false;
            player2.isDie = false;
            movementSpeed = 5;
            playerInfo.setText("Player" + 2 + " wins!");
            go_to_results(stage);
            return;
        }else if(player2.getTranslateY() > Game.HEIGHT || player2.isDie){
            monsterAI.works = false;
            player.isDie = false;
            player2.isDie = false;
            movementSpeed = 5;
            playerInfo.setText("Player" + 1 + " wins!");
            go_to_results(stage);
            return;
        }
        double x = who_is_first(player, player2).getTranslateX();
        int step_ = WIDTH / 3;
        if(x >= maxDistance){
            double delta = Math.abs(player.getTranslateX() - player2.getTranslateX());
            if(who_is_first(player, player2) == player){
                player.setTranslateX(step_);
                player2.setTranslateX(step_ - delta);
            }else{
                player.setTranslateX(step_ - delta);
                player2.setTranslateX(step_);
            }
            rootSoloMode.setTranslateX(0);
        }
        if(x > step_) {
            rootCoopMode.setTranslateX(-(x - step_));
            distanse.setTranslateX(x + WIDTH / 2);
        }
    }

    void go_to_menu(Stage stage){
        lblResults.setText(records);
        rootStartDisplay.getChildren().addAll(background, menu, lblResults);
        background.setOpacity(0.5);
        stage.setScene(sceneStartDisplay);
        if(sceneNumber == 1) rootSoloMode.getChildren().clear();
        if(sceneNumber == 2) rootCoopMode.getChildren().clear();
        if(sceneNumber == 4) rootResults.getChildren().clear();
        sceneNumber = 0;
        arrBlock.clear();
    }

    void go_to_results(Stage stage){
        rootResults.getChildren().addAll(backGameOver, playerInfo, pressSpace);
        stage.setScene(sceneResults);
        if(sceneNumber == 1) rootSoloMode.getChildren().clear();
        if(sceneNumber == 2) rootCoopMode.getChildren().clear();
        sceneNumber = 4;
    }

    Character who_is_first(Character pl1, Character pl2){
        if(pl1.getTranslateX() >= pl2.getTranslateX()) return pl1;
        else return pl2;
    }

    void sendData(Object obj){
        try {
            out.writeObject(obj);
            out.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
