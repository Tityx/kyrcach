import javafx.animation.AnimationTimer;

public class MonsterAI implements Runnable {

    private Thread thread;
    public boolean works = false;
    private double movementSpeed = 1, distance = 100;
    private boolean moveLeft = true;

    MonsterAI(){
        thread = new Thread(this, "MonsterAI");
        thread.start();
    }

    @Override
    public void run() {
        AnimationTimer timer = new AnimationTimer() {
            @Override
            public void handle(long now) {
                if(works){
                    for(Block monster : Game.arrBlock){
                        if(monster.blockType == Block.BlockType.MONSTER) {
                            double x = monster.getTranslateX();
                            if (x <= monster.startX) moveLeft = false;
                            else if (x >= monster.startX + distance*2) moveLeft = true;
                            if (moveLeft) monster.moveX(-movementSpeed);
                            else monster.moveX(movementSpeed);
                        }
                    }
                }
            }
        };
        timer.start();
    }
}
