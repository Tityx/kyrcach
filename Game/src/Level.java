import javafx.scene.layout.Pane;

import java.util.Random;

public class Level extends Pane{

    private int lvl;
    public static String[] LEVEL0 = new String[]{
            "0",
            "0",
            "0",
            "0",
            "0",
            "0",
            "0",
            "0000000000000000000000000000000000000",
            "0000000000000000000000000011100000",
            "00000000000000000000000001000100000000000000000000000000000000000000000",
            "000000000000000000000000000000000000000000000000000000000000000000000",
            "1111111111111111111111111111111011111111111111111111101111111111101111111111111111111111111111"
           /*1111111111111111111111111*/                                        /*1111111111111111111111111*/
    };
    public static String[][] levels = new String[][]{LEVEL0};

    Level(int level, Pane root){
        lvl = level;
        build_lvl(root);
    }

    void build_lvl(Pane root){
        for(int i = 0; i < levels[lvl].length; i++){
            String line = levels[lvl][i];
            for(int j = 0; j < line.length(); j++){
                char c = line.charAt(j);
                int random = new Random().nextInt(100);
                if(i >= levels[lvl].length - 3 && i <= levels[lvl].length - 2 && j > 25 && j < levels[lvl][levels[lvl].length - 1].length() - 25
                && random <= 20 && j % 5 == 0 && c != '1'){
                    c = '4';
                }
                switch (c){
                    case '0':
                        break;
                    case '1':
                        Block platform = new Block(Block.BlockType.PLATFORM,j * Game.BLOCK_SIZE, i * Game.BLOCK_SIZE);
                        root.getChildren().add(platform);
                        break;
                    case '2':
                        Block let = new Block(Block.BlockType.LET,j * Game.BLOCK_SIZE, i * Game.BLOCK_SIZE);
                        root.getChildren().add(let);
                        break;
                    case '3':
                        Block brick = new Block(Block.BlockType.BRICK,j * Game.BLOCK_SIZE, i * Game.BLOCK_SIZE);
                        root.getChildren().add(brick);
                        break;
                    case '4':
                        Block monster = new Block(Block.BlockType.MONSTER,j * Game.BLOCK_SIZE, i * Game.BLOCK_SIZE);
                        root.getChildren().add(monster);
                        break;
                }
            }
        }
    }
}
