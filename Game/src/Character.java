import javafx.geometry.Rectangle2D;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Pane;
import javafx.util.Duration;
import java.io.Serializable;

public class Character extends Pane {

    private Pane root;
    private ImageView player;
    private int offsetYRun, offsetYJump, columnsRun, columnsJump;
    public Animation animation;
    public int gravity = 10;
    public boolean canJump = true, isDie = false;

    Character(ImageView imageView, int columnsRun, int columnsJump, int offsetX, int offsetYRun, int offsetYJump, int width, int height){
        this.offsetYJump = offsetYJump;
        this.offsetYRun = offsetYRun;
        this.columnsRun = columnsRun;
        this.columnsJump = columnsJump;
        player = imageView;
        player.setFitHeight(Game.PLAYER_SIZE);
        player.setFitWidth(Game.PLAYER_SIZE);
        player.setViewport(new Rectangle2D(offsetX,offsetYRun,width,height));
        animation = new Animation(player, Duration.millis(400),  columnsRun, offsetX, offsetYRun, width, height);
        getChildren().add(player);
    }

    void moveX(double x){
        for(int i = 0; i < x; i++){
            for(Block block : Game.arrBlock){
                if(getBoundsInParent().intersects(block.getBoundsInParent())){
                    if(getTranslateX() + Game.PLAYER_SIZE == block.getTranslateX() && getTranslateY() != block.getTranslateY() + block.block_size
                    && getTranslateY()  + Game.PLAYER_SIZE != block.getTranslateY()){
                        checkBlock(block, false);
                        return;
                    }
                }
            }
            setTranslateX(getTranslateX() + 1);
        }
    }
    void moveY(double y){
        int UpDown = y > 0 ? 1 : -1;
        for(int i = 0; i < Math.abs(y); i++){
            for(Block block : Game.arrBlock){
                if(getBoundsInParent().intersects(block.getBoundsInParent())){
                    if(UpDown == -1) {
                        if (getTranslateY() == block.getTranslateY() + block.block_size) {
                            checkBlock(block, true);
                            return;
                        }
                    }
                    if(UpDown == 1){
                        if (getTranslateY() + Game.PLAYER_SIZE == block.getTranslateY()) {
                            canJump = true;
                            checkBlock(block, true);
                            animation.setColumns(columnsRun);
                            animation.setOffsetY(offsetYRun);
                            return;
                        }
                    }
                }
            }
            setTranslateY(getTranslateY() + UpDown);
        }
    }
    void jump(){
        if(canJump){
            gravity = -20;
            canJump = false;
            animation.setColumns(columnsJump);
            animation.setOffsetY(offsetYJump);
        }
    }
    void checkBlock(Block block, boolean b){
        if(b) gravity = 10;
        if(block.blockType == Block.BlockType.MONSTER || block.blockType == Block.BlockType.LET) {
            isDie = true;
        }
    }
    void setRoot(Pane root){
        this.root = root;
    }
}
