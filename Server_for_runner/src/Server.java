import javax.swing.*;
import java.awt.event.*;
import java.net.*;
import java.io.*;
import java.util.*;

public class Server extends JFrame implements Runnable {

    public static JTextArea consoleText = new JTextArea();
    private Thread thread;
    private ServerSocket server;
    private Socket client;
    public static LinkedList<MonoServer> serverList = new LinkedList<>();
    public static LinkedHashMap<String, Integer> recordsNames = new LinkedHashMap<>();
    private String fileName = "records.ser";

    public Server(){
        super("Server");
        thread = new Thread(this, "Server");
        this.setSize(600, 400);
        this.setDefaultCloseOperation(this.EXIT_ON_CLOSE);
        consoleText.setLineWrap(true);
        this.add(new JScrollPane(consoleText));
        this.setVisible(true);
        thread.start();
        load_records();
    }

    @Override
    public void run() {
        this.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                super.windowClosing(e);
                save_records();
            }
        });
        try {
            server = new ServerSocket(1211);
            while(true){
                client = server.accept();
                serverList.add(new MonoServer(client));
            }
        }catch (IOException e) {
            e.printStackTrace();
        }
    }
    void load_records(){
        try{
            FileInputStream fileInputStream = new FileInputStream(fileName);
            ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream);
            SaveRecords saveRecords = (SaveRecords) objectInputStream.readObject();
            recordsNames = saveRecords.records;
            objectInputStream.close();
            fileInputStream.close();
        }catch (IOException e){
            e.printStackTrace();
        }catch (ClassNotFoundException e){
            e.printStackTrace();
        }
    }
    void save_records(){
        try {
            FileOutputStream outputStream = new FileOutputStream(fileName);
            ObjectOutputStream objectOutputStream = new ObjectOutputStream(outputStream);
            SaveRecords saveRecords = new SaveRecords(recordsNames);
            objectOutputStream.writeObject(saveRecords);
            objectOutputStream.close();
            outputStream.close();
        }catch (IOException e){
            e.getMessage();
        }
    }
}