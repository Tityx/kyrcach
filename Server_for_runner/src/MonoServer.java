import java.io.*;
import java.net.*;
import java.util.*;

public class MonoServer implements Runnable{

    private Thread thread;
    private Socket client;
    private ObjectOutputStream out;
    private ObjectInputStream in;

    MonoServer(Socket client) throws IOException{
        this.client = client;
        thread = new Thread(this, "MonoServer");
        print_message("Клиент " + client.getRemoteSocketAddress().toString() + " подключился");
        print_message("Сейчас в сети " + (Server.serverList.size()+1) + " клиентов");
        out = new ObjectOutputStream(client.getOutputStream());
        in = new ObjectInputStream(client.getInputStream());
        thread.start();
    }

    @Override
    public void run(){
        try{

            while (true){
                Object tmp = in.readObject();
                if(tmp.equals("quite")){
                    close_client();
                    break;
                }else if(tmp.equals("send records")){
                    String key = (String)in.readObject();
                    int value = (int)in.readObject();
                    put(key, value);
                    sendData("send records");
                    sendData(map_to_string());
                }
            }
        }catch (IOException e) {
            e.printStackTrace();
        }catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }
    void close_client(){
        print_message("Клиент " + client.getRemoteSocketAddress().toString() + " отключился");
        Server.serverList.remove(this);
        print_message("Осталось " + Server.serverList.size() + " клиентов");
        try {
            in.close();
            out.close();
            client.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    void sendData(Object obj){
        try {
            out.writeObject(obj);
            out.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    void print_message(String message){
        Server.consoleText.append(message + "\n");
    }

    void put(String key, int value){
        String tmpStr = key;
        int count = 1;
        while(Server.recordsNames.containsKey(key)) {
            key = tmpStr + "(" + count + ")";
            count++;
        }
        if(Server.recordsNames.size() < 10){
            Server.recordsNames.put(key, value);
            sort();
        }else{
            int minValue = 1000;
            String keyStr = "";
            for(Map.Entry<String, Integer> entry : Server.recordsNames.entrySet()){
                if(entry.getValue() < minValue){
                    minValue = entry.getValue();
                    keyStr = entry.getKey();
                }
            }
            if(value > minValue){
                Server.recordsNames.remove(keyStr);
                Server.recordsNames.put(key, value);
            }
            sort();
        }
    }

    void sort(){
        String[] keyArr = new String[Server.recordsNames.size()];
        int[] valueArr = new int[Server.recordsNames.size()];
        int count = 0;
        for(Map.Entry<String, Integer> entry : Server.recordsNames.entrySet()){
            keyArr[count] = entry.getKey();
            valueArr[count] = entry.getValue();
            count++;
        }
        for(int i = valueArr.length - 1; i > 0; i--){
            for(int j = 0; j < i; j++){
                if(valueArr[j] < valueArr[j + 1]){
                    int tmpV = valueArr[j];
                    valueArr[j] = valueArr[j + 1];
                    valueArr[j + 1] = tmpV;
                    String tmpK = keyArr[j];
                    keyArr[j] = keyArr[j + 1];
                    keyArr[j + 1] = tmpK;
                }
            }
        }
        Server.recordsNames.clear();
        for(int i = 0; i < valueArr.length; i++){
            Server.recordsNames.put(keyArr[i], valueArr[i]);
        }
    }

    String map_to_string(){
        int count = 1;
        String records = "Distanse:\n";
        for(Map.Entry<String, Integer> entry : Server.recordsNames.entrySet()){
            records += (count++) + ". " + entry.getKey() + " - " + entry.getValue() + "\n";
        }
        return records;
    }
}
